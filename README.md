# Anotações Gerais do Minicurso de Arduino

Material para o curso de arduino planejado para a semana do curso de computação da UFCA em 2022.
(**Seguir o material do** [franzininho](https://docs.franzininho.com.br/docs/franzininho-wifi/exemplos-espidf/primeiros-passos))

## Publico Alvo
Qualquer pessoa que tenha um pequena noção de programação em qualquer linguagem.

## Tópicos

### Materiais
* Protoboard

### Componentes
* Resistores
* Chaves e botões
    - Push button
    - Chave interruptor
    - Chave Magnética Reed Switch
* LEDs
* Potenciômetro
* LDR
* Relê
* Motor
* Transdutor Piezoelétrico
* Buzzer
* Acelerômetro
* Sensor de distância
* Display de 7 segmentos (1 ou 3 dígitos?)
* Termistor(?)


### Placa Arduino
* Histórico
* Finalidade
* Família Arduino
* Descrição das características
   - Tensões de entrada 
   - Tensões de saída
   - Microcontrolador
   - 
* Shields 
* Módulos
* Comunicação serial

### A Linguagem
* Entradas e saídas digitais
* Entradas e saídas analógicas
* Multiplexação
* Comunicação serial
* delay e millis
* 

### Conceitos de física/eletrônica
* Divisor de tensão
* Entradas e saídas digitais
* Entradas e saídas analógicas
* Multiplexação

### Projetos
* Blink (comparar com um blink feito analogicamente)
* Controlar velocidade do blink pelo potenciômetro
* Controlar velocidade do blink com push buttons (+ e -)
* Controlar luminosidade do LED pelo LDR (?)
* Controlar LED pelo computador ou celular
* Receber leitura de um sensor no computador ou celular
* (Blink avançado) Acender um LED a partir de um post no twitter, telegram ou outra rede
* (LDR avançado) Gerar um post a partir no twitter ou telegram ou outra rede baseado na leitura de um sensor
* Gerar alarme baseado na temperatura ou luminosidade
* Uma musiquinha automática e/ou pelos botões
* Controlar frequência da sirene pelos botões

### Referências
* https://arduinogetstarted.com/tutorials/arduino-led-blink
*

### Ideias
* Construir/preparar as plaquinhas/protoboars já com os componentes na posição certa.
* Mostrar alguns circuitos mais complexos pra dar um gostinho de quero mais. Pode ser até com ESP32
  - Pode-se se fazer umas cenas dos próximos capítulos (cursos mais avançados)
* Usar esquemas no tinkercad.com
* Fazer um servidor web para demonstração, controlando leds e outros componentes
* Mostrar alguns exemplos em micropython com ESP32 e RP2040 
* https://www.youngwonks.com/resources/python-cheatsheet (Fazer um pro Arduino)
* https://store.youngwonks.com/ (Fazer kits e brindes?)
