## Pinagem da nossa plaquinha

| Componente | Pino |
--- | --- | --- |
|LED_VERDE | 6 |
|LED_AMARELO | 5 -> 3|
|LED_VERMELHO | 9 |
|LED_AZUL | 10 -> 11 |
|BUZZER | 3 |
|POTENCIÔMETRO | A0 |
|LDR | A1 |
|BOTAO_1 | 12 | 
|BOTAO_2 | 8 | 
|BOTAO_3 | 7 | 
|BOTAO_4 | 4 | 



