/*
  Blink controlado analogicamente pelo potenciômetro

  Lê a entrada analógica A0, converte o resultado para voltagem e mostra o resultado no Monitor Serial.

*/
const int POTENCIOMETRO = A0;
const int LED = 10;

int valorDoSensor = 0;

// Função de configuração inicial. Ela é executada somente uma vez (ao ligar ou resetar a placa).
void setup() {
  // Inicializa o pino digital LED como saída.
  pinMode(LED, OUTPUT);
}

// A função loop é sempre invocada e invocada para sempre, pelo menos enquanto o microcontrolador estiver ligado.
void loop() {
  // Lê a entrada do potenciômetro
  valorDoSensor = analogRead(POTENCIOMETRO);
  digitalWrite(LED, HIGH);
  delay(valorDoSensor);
  digitalWrite(LED, LOW);
  delay(valorDoSensor);
}
