
// digital pin 2 has a pushbutton attached to it. Give it a name:
const int BOTAO_1 = 4;
const int BOTAO_2 = 7;
const int BOTAO_3 = 8;
const int BOTAO_4 = 12;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(BOTAO_1, INPUT_PULLUP);
  pinMode(BOTAO_2, INPUT_PULLUP);
  pinMode(BOTAO_3, INPUT_PULLUP);
  pinMode(BOTAO_4, INPUT_PULLUP);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  int estado_do_botao_1 = digitalRead(BOTAO_1);
  int estado_do_botao_2 = digitalRead(BOTAO_2);
  int estado_do_botao_3 = digitalRead(BOTAO_3);
  int estado_do_botao_4 = digitalRead(BOTAO_4);
  // print out the state of the button:
  if (estado_do_botao_1 == 0) {
    Serial.print('a');
    delay(200);  // delay in between reads for stability
  }
  if (estado_do_botao_2 == 0) {
    Serial.print('s');
    delay(200);  // delay in between reads for stability
  }
  if (estado_do_botao_3 == 0) {
    Serial.print('d');
    delay(200);  // delay in between reads for stability
  }
  if (estado_do_botao_4 == 0) {
    Serial.print('f');
    delay(200);  // delay in between reads for stability
  }
  
  delay(1); // delay in between reads for stability
}
