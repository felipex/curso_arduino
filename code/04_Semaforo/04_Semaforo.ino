/*
*/

const int LED_VERDE = 10;
const int LED_AMARELO = 9;
const int LED_VERMELHO = 6;

const int TEMPO_DO_VERDE = 4000;     // 4000 milissegundos = 4 segundos
const int TEMPO_DO_AMARELO = 2000;   // 2000 milissegundos = 2 segundos
const int TEMPO_DO_VERMELHO = 6000;  // 6000 milissegundos = 6 segundos

// Função de configuração inicial. Ela é executada somente uma vez (ao ligar ou resetar a placa).
void setup() {
  // Inicializa os pinos digitais LED_VERDE, LED_AMARELO e LED_VERMELHO como saída.
  pinMode(LED_VERDE, OUTPUT);
  pinMode(LED_AMARELO, OUTPUT);
  pinMode(LED_VERMELHO, OUTPUT);
}

// A função loop é sempre invocada e invocada para sempre, pelo menos enquanto o microcontrolador estiver ligado.
void loop() {
  // Pode passar
  digitalWrite(LED_VERDE, HIGH);  
  digitalWrite(LED_AMARELO, LOW); 
  digitalWrite(LED_VERMELHO, LOW); 
  delay(TEMPO_DO_VERDE);       

  // Atenção  
  digitalWrite(LED_VERDE, LOW);  
  digitalWrite(LED_AMARELO, HIGH); 
  digitalWrite(LED_VERMELHO, LOW); 
  delay(TEMPO_DO_AMARELO);       

  // Pare
  digitalWrite(LED_VERDE, LOW);  
  digitalWrite(LED_AMARELO, LOW); 
  digitalWrite(LED_VERMELHO, HIGH); 
  delay(TEMPO_DO_VERMELHO);       
  
}
