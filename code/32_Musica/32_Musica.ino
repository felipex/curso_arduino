/*

  Melody

  Plays a melody

  circuit:

  - 8 ohm speaker on digital pin 8

  created 21 Jan 2010

  modified 30 Aug 2011

  

  This example code is in the public domain.

  http://www.arduino.cchttps://www.arduino.cc/en/Tutorial/Tone

*/
// https://github.com/robsoncouto/arduino-songs
#include "pitches.h"

#define REST      0

const int BUZZ = 3;

// notes in the melody:
int melody[] = {

  NOTE_G4, NOTE_A4, NOTE_B4, NOTE_D5, NOTE_D5, NOTE_B4, 
  NOTE_C5, NOTE_C5, NOTE_G4, NOTE_A4,
  NOTE_B4, NOTE_D5, NOTE_D5, NOTE_C5,

  NOTE_B4, REST, NOTE_G4, NOTE_G4, NOTE_A4,
  NOTE_B4, NOTE_D5, REST, NOTE_D5, NOTE_C5, NOTE_B4,
  NOTE_G4, NOTE_C5, REST, NOTE_C5, NOTE_B4, NOTE_A4, 

  NOTE_A4, NOTE_B4, REST, NOTE_B4, NOTE_A4, NOTE_G4,
  NOTE_G4, REST, NOTE_G4, NOTE_G4, NOTE_A4,
  NOTE_B4, NOTE_D5, REST, NOTE_D5, NOTE_C5, NOTE_B4,

  NOTE_G4, NOTE_C5, REST, NOTE_C5, NOTE_B4, NOTE_A4,
  NOTE_A4, NOTE_B4, REST, NOTE_B4, NOTE_A4, NOTE_G4,
  NOTE_G4, NOTE_F5, NOTE_D5, NOTE_E5, NOTE_C5, NOTE_D5, NOTE_B4,

  NOTE_C5, NOTE_A4, NOTE_B4, NOTE_G4, NOTE_A4, NOTE_G4, NOTE_E4, NOTE_G4,
  NOTE_G4, NOTE_F5, NOTE_D5, NOTE_E5, NOTE_C5, NOTE_D5, NOTE_B4,
  NOTE_C5, NOTE_A4, NOTE_B4, NOTE_G4, NOTE_A4, NOTE_G4, NOTE_E4, NOTE_G4,
  NOTE_G4, REST
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {

  8, 8, 4, 4, 4, 4, 
  4, 2, 8, 8,
  4, 4, 4, 4,

  2, 8, 8, 8, 8,
  4, 4, 8, 8, 8, 8,
  4, 4, 8, 8, 8, 8,

  4, 4, 8, 8, 8, 8,
  2, 8, 8, 8, 8,
  4, 4, 8, 8, 8, 8,

  4, 4, 8, 8, 8, 8,
  4, 4, 8, 8, 8, 8,
  4, 8, 8, 8, 8, 8, 8,

  8, 8, 8, 8, 8, 8, 8, 8,
  4, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8,
  -2, 4  

};
int noteDuration = 0;

void setup() {

  // iterate over the notes of the melody:
  int length = sizeof(melody) / sizeof(int);
  for (int thisNote = 0; thisNote < length; thisNote++) {

    // to calculate the note duration, take one second divided by the note type.

    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.

    if (noteDurations[thisNote] > 0) {
      noteDuration = 1000 / noteDurations[thisNote];
    } else if (noteDurations[thisNote] < 0) {
      noteDuration = 1000 / abs(noteDurations[thisNote]);
      noteDuration *= 1.5;
    }

    tone(BUZZ, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.

    // the note's duration + 30% seems to work well:

    int pauseBetweenNotes = noteDuration * 2;

    delay(pauseBetweenNotes);

    // stop the tone playing:

    noTone(BUZZ);

  }
}

void loop() {

  // no need to repeat the melody.
}