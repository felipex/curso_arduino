/*
  Lê o valor do potenciômetro e liga um bateria de leds dependendo do valor lido.

*/
const int POTENCIOMETRO = A1;
const int LED_1 = 6;
const int LED_2 = 9;
const int LED_3 = 10;
const int LED_4 = 11;

int valorDoSensor = 0;

// Função de configuração inicial. Ela é executada somente uma vez (ao ligar ou resetar a placa).
void setup() {
  // Inicializa os pinos digitals dos LEDs como saída.
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(LED_3, OUTPUT);
  pinMode(LED_4, OUTPUT);
}

// A função loop é sempre invocada e invocada para sempre, pelo menos enquanto o microcontrolador estiver ligado.
void loop() {
  // Lê a entrada do potenciômetro
  valorDoSensor = analogRead(POTENCIOMETRO);
  if (valorDoSensor > 100) digitalWrite(LED_1, HIGH);
  if (valorDoSensor > 400) digitalWrite(LED_2, HIGH);
  if (valorDoSensor > 800) digitalWrite(LED_3, HIGH);
  if (valorDoSensor > 1000) digitalWrite(LED_4, HIGH);

  if (valorDoSensor <= 100) digitalWrite(LED_1, LOW);
  if (valorDoSensor <= 400) digitalWrite(LED_2, LOW);
  if (valorDoSensor <= 800) digitalWrite(LED_3, LOW);
  if (valorDoSensor <= 1000) digitalWrite(LED_4, LOW);
  
  delay(1);
}
