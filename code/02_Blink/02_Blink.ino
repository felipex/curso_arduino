/*
*/


const int LED_VERDE = 6;

// Função de configuração inicial. Ela é executada somente uma vez (ao ligar ou resetar a placa).
void setup() {
  // Inicializa o pino digital LED_VERDE como saída.
  pinMode(LED_VERDE, OUTPUT);
}

// A função loop é sempre invocada e invocada para sempre, pelo menos enquanto o microcontrolador estiver ligado.
void loop() {
  digitalWrite(LED_VERDE, HIGH);  // Liga o LED. 
  delay(1000);                    // Espera por um segundo
  digitalWrite(LED_VERDE, LOW);   // Desliga o LED
  delay(1000);                    // Espera por um segundo
}
