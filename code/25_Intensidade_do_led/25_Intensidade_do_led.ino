/*
  Muda a intensidade do LED no tempo.

*/
const int LED = 10;
const float periodo = 0.5; // Em segundos.

// Função de configuração inicial. Ela é executada somente uma vez (ao ligar ou resetar a placa).
void setup() {
  // Inicializa o pinos PWM do LED como saída.
  pinMode(LED, OUTPUT);
}

// A função loop é sempre invocada e invocada para sempre, pelo menos enquanto o microcontrolador estiver ligado.
void loop() {
  float tempo = millis() / 1000.0;
  int intensidade = 127 +  127 * cos((2 * 3.14 / periodo) * tempo);
  analogWrite(LED, intensidade);
  delay(30);
}
