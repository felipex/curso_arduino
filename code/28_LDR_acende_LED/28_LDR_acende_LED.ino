/*
  Leitura Analógica

  Lê a entrada analógica A1 e mostra o resultado no Monitor Serial.
  
*/
const int LDR = A1;
const int LED = 6;

// Função de configuração inicial. Ela é executada somente uma vez (ao ligar ou resetar a placa).
void setup() {
  // Inicializa o pino digital LED_VERDE como saída.
  Serial.begin(9600);

  pinMode(LED, OUTPUT);
}

// A função loop é sempre invocada e invocada para sempre, pelo menos enquanto o microcontrolador estiver ligado.
void loop() {
  // Lê a entrada analógica LDR.
  int valorDoSensor = analogRead(LDR);
  // Mostra o valor lido.
  Serial.println(valorDoSensor);
  if (valorDoSensor > 500) {
    digitalWrite(LED, HIGH);  
  } else {
    digitalWrite(LED, LOW);
  }
  delay(1);  // Esperar um tempinho entre as leituras para dar um certa estabilidade.
}
