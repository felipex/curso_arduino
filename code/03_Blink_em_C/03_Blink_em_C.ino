#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>

int main(){
     // BIT 76543210
  DDRB  = 0b00100000; //Pino 13 como saída e resto como entrada.
  PORTB = 0b00000000; //Todos os pinos em nível baixo (LOW)
  while( true ){
    //      0b76543210
    PORTB = 0b00100000;
    _delay_ms( 100 );
    PORTB = 0b00000000;
    _delay_ms( 100 );
  }
  return 0;
}