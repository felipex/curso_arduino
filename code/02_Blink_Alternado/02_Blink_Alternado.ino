/*
*/


const int LED_VERDE = 10;
const int LED_AMARELO = 9;

// Função de configuração inicial. Ela é executada somente uma vez (ao ligar ou resetar a placa).
void setup() {
  // Inicializa os pinos digitais LED_VERDE e LED_AMARELO como saída.
  pinMode(LED_VERDE, OUTPUT);
  pinMode(LED_AMARELO, OUTPUT);
}

// A função loop é sempre invocada e invocada para sempre, pelo menos enquanto o microcontrolador estiver ligado.
void loop() {
  digitalWrite(LED_VERDE, HIGH);    // Liga o LED VERDE. 
  digitalWrite(LED_AMARELO, LOW);  // Desliga o LED AMARELO. 
  delay(500);                      // Espera por um segundo
  digitalWrite(LED_VERDE, LOW);     // Desliga o LED
  digitalWrite(LED_AMARELO, HIGH);   // Desliga o LED
  delay(500);                      // Espera por um segundo
}
