
// digital pin 2 has a pushbutton attached to it. Give it a name:
const int BOTAO_1 = 4;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(BOTAO_1, INPUT_PULLUP);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  int estado_do_botao = digitalRead(BOTAO_1);
  // print out the state of the button:
  Serial.println(estado_do_botao);
  delay(1);  // delay in between reads for stability
}
