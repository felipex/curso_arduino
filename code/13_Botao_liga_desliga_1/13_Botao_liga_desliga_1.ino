
// digital pin 2 has a pushbutton attached to it. Give it a name:
const int BOTAO = 12;

const int LED = 10;

int estado_do_botao = 0;

// the setup routine runs once when you press reset:
void setup() {

  // make the pushbutton's pin an input:
  pinMode(BOTAO, INPUT_PULLUP);

  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  estado_do_botao = digitalRead(BOTAO);
  
  if (estado_do_botao == LOW) {
    digitalWrite(LED, HIGH);
  } else {
    digitalWrite(LED, LOW);
  }
  delay(10);  // delay in between reads for stability
}
