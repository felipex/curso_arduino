
// digital pin 2 has a pushbutton attached to it. Give it a name:
const int BOTAO_MAIS = 12;
const int BOTAO_MENOS = 8;

const int LED = 10;

int estado_do_botao_mais = HIGH;
int estado_do_botao_menos = HIGH;
int delay_do_blink = 1000;
bool led_aceso = false;

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long tempoAnterior = 0;  
unsigned long tempoAtual = 0;

// the setup routine runs once when you press reset:
void setup() {

  // make the pushbutton's pin an input:
  pinMode(BOTAO_MAIS, INPUT_PULLUP);
  pinMode(BOTAO_MENOS, INPUT_PULLUP);

  pinMode(LED, OUTPUT);

  tempoAtual = millis();
  tempoAnterior = tempoAtual;
}

// the loop routine runs over and over again forever:
void loop() {
  tempoAtual = millis();
  if (tempoAtual - tempoAnterior >= delay_do_blink) {
    tempoAnterior = tempoAtual;
    if (led_aceso) {
      digitalWrite(LED, LOW);
      led_aceso = false;
    } else {
      digitalWrite(LED, HIGH);      
      led_aceso = true;
    }
  }
  
  // read the input pin:
  estado_do_botao_mais = digitalRead(BOTAO_MAIS);
  estado_do_botao_menos = digitalRead(BOTAO_MENOS);
  if (estado_do_botao_mais == LOW) {    
    delay_do_blink += 100;
    delay(100);  // delay in between reads for stability
  }
  if (estado_do_botao_menos == LOW) {    
    delay_do_blink -= 100;
    delay(100);  // delay in between reads for stability
  }
}
