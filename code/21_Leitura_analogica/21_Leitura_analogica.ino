/*
  Leitura Analógica

  Lê a entrada analógica A0 e mostra o resultado no Monitor Serial.
  
*/

// Função de configuração inicial. Ela é executada somente uma vez (ao ligar ou resetar a placa).
void setup() {
  // Inicializa o pino digital LED_VERDE como saída.
  Serial.begin(9600);
}

// A função loop é sempre invocada e invocada para sempre, pelo menos enquanto o microcontrolador estiver ligado.
void loop() {
  // Lê a entrada analógica 0.
  int valorDoSensor = analogRead(A0);
  // Mostra o valor lido.
  Serial.println(valorDoSensor);
  delay(1);  // Esperar um tempinho entre as leituras para dar um certa estabilidade.
}
