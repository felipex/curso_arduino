
// digital pin 2 has a pushbutton attached to it. Give it a name:
const int BOTAO_LIGA = 12;
const int BOTAO_DESLIGA = 8;

const int LED = 10;

int estado_do_botao_liga = 0;
int estado_do_botao_desliga = 0;

// the setup routine runs once when you press reset:
void setup() {

  // make the pushbutton's pin an input:
  pinMode(BOTAO_LIGA, INPUT_PULLUP);
  pinMode(BOTAO_DESLIGA, INPUT_PULLUP);

  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  estado_do_botao_liga = digitalRead(BOTAO_LIGA);
  estado_do_botao_desliga = digitalRead(BOTAO_DESLIGA);
  if (estado_do_botao_liga == LOW) {    
    digitalWrite(LED, HIGH);
    delay(100);  // delay in between reads for stability
  }
  if (estado_do_botao_desliga == LOW) {    
    digitalWrite(LED, LOW);
    delay(100);  // delay in between reads for stability
  }
  
}
