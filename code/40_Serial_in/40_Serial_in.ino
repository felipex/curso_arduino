/*
  Input Pull-up Serial

  This example demonstrates the use of pinMode(INPUT_PULLUP). It reads a digital
  input on pin 2 and prints the results to the Serial Monitor.

  The circuit:
  - momentary switch attached from pin 2 to ground
  - built-in LED on pin 13

  Unlike pinMode(INPUT), there is no pull-down resistor necessary. An internal
  20K-ohm resistor is pulled to 5V. This configuration causes the input to read
  HIGH when the switch is open, and LOW when it is closed.

  created 14 Mar 2012
  by Scott Fitzgerald

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/InputPullupSerial
*/

int incomingByte = 0; // for incoming serial data
int sensorVal = 0;

const int LED_AZUL = 6;
const int LED_VERMELHO = 9;
const int LED_AMARELO = 10;
const int LED_VERDE = 11;

void inicialidaLed(int led) {
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);
}

void ligaDesliga(int led) {
    int estado = digitalRead(led);
    digitalWrite(led, not estado);
}

void setup() {
  //start serial connection
  Serial.begin(9600);
  //configure pin 2 as an input and enable the internal pull-up resistor
  pinMode(7, INPUT_PULLUP);
  
  inicialidaLed(LED_AZUL);
  inicialidaLed(LED_VERMELHO);
  inicialidaLed(LED_AMARELO);
  inicialidaLed(LED_VERDE);
  
}

void loop() {
  //read the pushbutton value into a variable
  //int sensorVal = digitalRead(7);
  //print out the value of the pushbutton
  //Serial.println(sensorVal);

  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();

    // say what you got:
    Serial.print("I received: ");
    Serial.println(char(incomingByte));
  }
  // Keep in mind the pull-up means the pushbutton's logic is inverted. It goes
  // HIGH when it's open, and LOW when it's pressed. Turn on pin 13 when the
  // button's pressed, and off when it's not:
  if (incomingByte == int('a')) {
    ligaDesliga(LED_AZUL);
  }
  if (incomingByte == int('s')) {
    ligaDesliga(LED_VERMELHO);
  }
  if (incomingByte == int('d')) {
    ligaDesliga(LED_AMARELO);
  }
  if (incomingByte == int('f')) {
    ligaDesliga(LED_VERDE);
  }
  delay(300);
}
