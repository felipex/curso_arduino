/*
  Muda a intensidade do LED no tempo.

*/
const int LED = 10;
const int POTENCIOMETRO = A0;

float intensidade = 0.5; 

// Função de configuração inicial. Ela é executada somente uma vez (ao ligar ou resetar a placa).
void setup() {
  // Inicializa o pinos PWM do LED como saída.
  pinMode(LED, OUTPUT);

  pinMode(POTENCIOMETRO, INPUT);
}

// A função loop é sempre invocada e invocada para sempre, pelo menos enquanto o microcontrolador estiver ligado.
void loop() {
  int valor_do_pot = analogRead(POTENCIOMETRO);
  intensidade = valor_do_pot * (255.0 / 1023.0);
  analogWrite(LED, intensidade);
  delay(5);
}
