/*
*/


const int LED_AZUL = 6;
const int LED_VERMELHO = 9;
const int LED_AMARELO = 10;
const int LED_VERDE = 11;

// Função de configuração inicial. Ela é executada somente uma vez (ao ligar ou resetar a placa).
void setup() {
  // Inicializa os pinos digitais LED_VERDE e LED_AMARELO como saída.
  pinMode(LED_AZUL, OUTPUT);
  pinMode(LED_VERMELHO, OUTPUT);
  pinMode(LED_AMARELO, OUTPUT);
  pinMode(LED_VERDE, OUTPUT);
}

// A função loop é sempre invocada e invocada para sempre, pelo menos enquanto o microcontrolador estiver ligado.
void loop() {
  digitalWrite(LED_VERDE, LOW);     // Liga o LED VERDE. 
  digitalWrite(LED_AZUL, HIGH);     // Desliga o LED AZUL. 
  delay(500);                       // Espera por um segundo
  digitalWrite(LED_AZUL, LOW);      // Desliga o LED AZUL
  digitalWrite(LED_VERMELHO, HIGH); // Liga o LED VERMELHO
  delay(500);                       // Espera por um segundo
  digitalWrite(LED_VERMELHO, LOW);  // Desliga o LED VERMELHO
  digitalWrite(LED_AMARELO, HIGH);  // Liga o LED AMARELO
  delay(500);                       // Espera por um segundo
  digitalWrite(LED_AMARELO, LOW);   // Desliga o LED AMARELO
  digitalWrite(LED_VERDE, HIGH);    // Liga o LED VERDE
  delay(500);                       // Espera por um segundo
}
