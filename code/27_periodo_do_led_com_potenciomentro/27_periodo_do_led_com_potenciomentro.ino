/*
  Muda a intensidade do LED no tempo e muda o periodo do blink de acordo com a leitura do potenciômetro.

*/
const int LED = 10;
const int POTENCIOMETRO = A0;

float periodo = 0; // Em segundos.

// Função de configuração inicial. Ela é executada somente uma vez (ao ligar ou resetar a placa).
void setup() {
  // Inicializa o pinos PWM do LED como saída.
  pinMode(LED, OUTPUT);
  pinMode(POTENCIOMETRO, INPUT);

  Serial.begin(9600);
}

// A função loop é sempre invocada e invocada para sempre, pelo menos enquanto o microcontrolador estiver ligado.
void loop() {
  float tempo = millis() / 1000.0;
  int valor_do_pot = analogRead(POTENCIOMETRO);
  periodo = valor_do_pot * (5.0 / 1023.0);
  Serial.println(periodo);
  int intensidade = 127 +  127 * cos((2.0 * 3.14 / periodo) * tempo);
  analogWrite(LED, intensidade);
  delay(10);
}
